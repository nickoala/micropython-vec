import vec.distance

a = [2,3]
b = [5,7]

print(vec.distance.manhattan(a,b))
print(vec.distance.euclidean(a,b))

a = [2.2, 3.3]
b = [5.1, 7.4]

print(vec.distance.manhattan(a,b))
print(vec.distance.euclidean(a,b))


# functional style to implement the same thing for checking
def manhattan2(a, b):
    return sum([abs(x-y) for x,y in zip(a,b)])

def euclidean2(a, b):
    return sum([(x-y)**2 for x,y in zip(a,b)])**(0.5)


a = [0.04402268, 1.373316, 1.949028, 0.01964175, 1.191686, 0.7282537]
b = [0.04722816, 1.397288, 1.93805, 0.02638826, 1.247303, 0.7502995]

print(vec.distance.manhattan(a,b))
print(manhattan2(a,b))

print(vec.distance.euclidean(a,b))
print(euclidean2(a,b))
